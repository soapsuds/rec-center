<!DOCTYPE html>
<html>
    <head>
        <?php include '../includes/ttu-head.html'; ?>
        <title>Staff | Rec Center | TTU</title>
        <meta name="Description" content="">
    </head>
    <body>
        <?php include '../includes/ttu-body-top.php'; ?>




        <!-- CONTENT START -->
        <section class="rec-header rec-header--large">
            <div class="rec-header__background">
              <div class="top-left-triangle"></div>
            </div>
            <h1>Professional Staff</h1>
            <div class="rec-header__image">
              <div class="top-left-triangle">
                <div class="triangle-image" style="background-image: url('../images/rec-center.jpg');"></div>
              </div>
            </div>
        </section>
        <section class="rec-staff-header">
            <h2>Our Staff</h2>
            <p>
                Spicy jalapeno bacon ipsum dolor amet tri-tip drumstick shoulder, beef bacon sausage ham hock strip steak. Prosciutto hamburger rump ham hock beef ribs leberkas corned beef ham meatloaf short ribs. Flank venison hamburger ball tip. Cupim drumstick beef ribs, turkey sausage fatback tongue ham hock picanha prosciutto. Kielbasa pork loin hamburger ground round tail pancetta shankle cow. Flank doner swine shoulder, spare ribs ham hock kielbasa sirloin bacon porchetta bresaola pig hamburger rump.
            </p>
        </section>

        <section class="staff">
            <h2>Director</h2>
            <div class="staff-member">
                <div class="staff-member__image">
                    <img src="../images/staff/peewee.gif" alt="Peewee Roberson">
                </div>
                <div class="staff-member__info">
                    <h4>Peewee Roberson</h4>
                    <p class="staff-member__title">Director, Recreational Sports</p>
                    <p class="staff-member__details">
                        B.S., Physical Education, Texas Tech University<br />
                        M.Ed., Physical Education, Texas Tech University<br />
                        C.R.S.S., National Intramural-Recreational Sports Association
                    </p>
                    <p class="staff-member__contact">
                        Student Rec Center, Rm. 202<br />
                        <a href="mailto:PeeWee.Roberson@ttu.edu">PeeWee.Roberson@ttu.edu</a><br />
                        <a href="tel:8067423351">(806) 742-3351</a>
                    </p>
                </div>
            </div>
        </section>

        <section class="staff">
            <h2>Associate Directors</h2>
            <div class="staff-member">
                <div class="staff-member__image">
                    <img src="../images/staff/betty.jpg" alt="Betty Blanton">
                </div>
                <div class="staff-member__info">
                    <h4>Betty Blanton</h4>
                    <p class="staff-member__title">
                        Associate Director,<br />
                        Fitness/Wellness, Outdoor Pursuits
                    </p>
                    <p class="staff-member__details">
                        B.S., Texas Christian University<br />
                        M.Ed., Colorado State University<br />
                        C.R.S.S., National Intramural-Recreational Sports Association<br />
                        ACSM, Health Fitness Specialist<br />
                        ACE Lifestyle Weight Management Specialist<br />
                        Certified Wellness Practitioner, National Wellness Institute
                    </p>
                    <p class="staff-member__contact">
                        Student Rec Center, Rm. 202<br />
                        <a href="mailto:Betty.Blanton@ttu.edu">Betty.Blanton@ttu.edu</a><br />
                        <a href="tel:8067423828">806.742.3828</a>
                    </p>
                </div>
            </div>
            <div class="staff-member">
                <div class="staff-member__image">
                    <img src="../images/staff/don.gif" alt="Don Davis">
                </div>
                <div class="staff-member__info">
                    <h4>Don Davis</h4>
                    <p class="staff-member__title">
                        Associate Director,<br />
                        Intramurals &amp; Sports Clubs
                    </p>
                    <p class="staff-member__details">
                        B.Sc., Education/English Literature - Texas Tech University<br />
                        M.Ed., Recreation and Leisure Service, Texas Tech University
                    </p>
                    <p class="staff-member__contact">
                        Student Rec Center, Rm. 202<br />
                        <a href="mailto:Don.Davis@ttu.edu">Don.Davis@ttu.edu</a><br />
                        <a href="tel:8067423351">806.742.3351</a>
                    </p>
                </div>
            </div>
            <div class="staff-member">
                <div class="staff-member__image">
                    <img src="../images/staff/steve.gif" alt="Steve Waden">
                </div>
                <div class="staff-member__info">
                    <h4>Steve Waden</h4>
                    <p class="staff-member__title">
                        Associate Director,<br />
                        Facilities &amp; SRC Operations
                    </p>
                    <p class="staff-member__details">
                        B.S.,  Recreation Management, Southern Illinois University<br />
                        M.S., Sports Management, Texas Tech University
                    </p>
                    <p class="staff-member__contact">
                        Student Rec Center, Rm. 202<br />
                        <a href="mailto:Steve.Waden@ttu.edu">Steve.Waden@ttu.edu</a><br />
                        <a href="8067423351">806.742.3351</a>
                    </p>
                </div>
            </div>
        </section>

        <section class="staff">
            <h2>Assistant Directors</h2>
            <div class="staff-member">
                <div class="staff-member__image">
                    <img src="../images/staff/jared.gif" alt="Jared McCauley">
                </div>
                <div class="staff-member__info">
                    <h4>Jared McCauley</h4>
                    <p class="staff-member__title">
                        Assistant Director, Intramurals
                    </p>
                    <p class="staff-member__details">
                        B.S., Sports Management, Baldwin-Wallace College<br />
                        M.Ed.,  Recreation Management, The University of Arkansas
                    </p>
                    <p class="staff-member__contact">
                        Student Rec Center, Rm. 203<br />
                        <a href="mailto:Jared.McCauley@ttu.edu">Jared.McCauley@ttu.edu</a><br />
                        <a href="tel:8067422945">806.742.2945</a>
                    </p>
                </div>
            </div>
            <div class="staff-member">
                <div class="staff-member__image">
                    <img src="../images/staff/kami.gif" alt="Kami White-Waden">
                </div>
                <div class="staff-member__info">
                    <h4>Kami White-Waden</h4>
                    <p class="staff-member__title">
                        Assistant Director,<br />
                        Fitness and Wellness
                    </p>
                    <p class="staff-member__details">
                        B.S., Exercise and Sport Science, Texas Tech University<br />
                        M.S., Sports Health/Sports Administration, Texas Tech University
                    </p>
                    <p class="staff-member__contact">
                        Fitness/Wellness Center<br />
                        <a href="mailto:kami.waden@ttu.edu">kami.waden@ttu.edu</a><br />
                        <a href="8067423828">806.742.3828</a>
                    </p>
                </div>
            </div>
            <div class="staff-member">
                <div class="staff-member__image">
                    <img src="../images/staff/scott.gif" alt="Scott Layher">
                </div>
                <div class="staff-member__info">
                    <h4>Scott Layher</h4>
                    <p class="staff-member__title">
                        Assistant Director, marketing
                    </p>
                    <p class="staff-member__details">
                        B.S., Marketing, University of Nebraska - Lincoln<br />
                        M.B.A., Business Administration, Southern Illinois University Edwardsville
                    </p>
                    <p class="staff-member__contact">
                        Student Rec Center, Rm. 202<br />
                        <a href="mailto:Scott.Layher@ttu.edu">Scott.Layher@ttu.edu</a><br />
                        <a href="tel:8067423351">806.742.3351</a> ext. 225
                    </p>
                </div>
            </div>
            <div class="staff-member">
                <div class="staff-member__image">
                    <img src="../images/staff/lindsay.gif" alt="Lindsay Anderson">
                </div>
                <div class="staff-member__info">
                    <h4>Lindsay Anderson</h4>
                    <p class="staff-member__title">
                        Assistant Director, Facilities
                    </p>
                    <p class="staff-member__details">
                        B.S., Exercise and Sport Science, Texas Tech University<br />
                        M.S., Exercise and Sport Science, Sport Management - Texas Tech Univeristy
                    </p>
                    <p class="staff-member__contact">
                        Student Rec Center, Rm. 202<br />
                        <a href="mailto:lindsay.gibson@ttu.edu">lindsay.gibson@ttu.edu</a><br />
                        <a href="tel:8067423351">806.742.3351</a>
                    </p>
                </div>
            </div>
            <div class="staff-member">
                <div class="staff-member__image">
                    <img src="../images/staff/brett.gif" alt="Brett Jackson">
                </div>
                <div class="staff-member__info">
                    <h4>Brett Jackson</h4>
                    <p class="staff-member__title">
                        Assistant Director,<br />
                        Intramurals and Special Events
                    </p>
                    <p class="staff-member__details">
                        B.S., Health, Exercise, and Sports Science, Texas Tech University
                    </p>
                    <p class="staff-member__contact">
                        Student Rec Center, Rm. 203<br />
                        <a href="mailto:brett.d.jackson@ttu.edu">brett.d.jackson@ttu.edu</a><br />
                        <a href="tel:8067422945">806.742.2945</a>
                    </p>
                </div>
            </div>
            <div class="staff-member">
                <div class="staff-member__image">
                    <img src="../images/staff/david.gif" alt="David Young">
                </div>
                <div class="staff-member__info">
                    <h4>David Young</h4>
                    <p class="staff-member__title">
                        Assistant Director,<br />
                        Outdoor Pursuits
                    </p>
                    <p class="staff-member__details">
                        B.S., Communication Studies, Texas Tech University
                    </p>
                    <p class="staff-member__contact">
                        Outdoor Pursuits Center<br />
                        <a href="mailto:David.Young@ttu.edu">David.Young@ttu.edu</a><br />
                        <a href="tel:8067422449">806.742.2449</a>
                    </p>
                </div>
            </div>
            <div class="staff-member">
                <div class="staff-member__image">
                    <img src="../images/staff/laura.gif" alt="Laura Thomas">
                </div>
                <div class="staff-member__info">
                    <h4>Laura Thomas</h4>
                    <p class="staff-member__title">
                        Assistant Director,<br />
                        Sport Clubs and Intramural Events
                    </p>
                    <p class="staff-member__details">
                        B.S., Recreation Administration- Texas State University, San Marcos<br />
                        M.E.D, College Student Personnel, Ohio University
                    </p>
                    <p class="staff-member__contact">
                        Student Rec Center, Rm. 203<br />
                        <a href="mailto:laura.w.thomas@ttu.edu">laura.w.thomas@ttu.edu</a><br />
                        <a href="tel:8067422945">806.742.2945</a>
                    </p>
                </div>
            </div>
            <div class="staff-member">
                <div class="staff-member__image">
                    <img src="../images/staff/kevinpearson.jpg" alt="Kevin Pearson">
                </div>
                <div class="staff-member__info">
                    <h4>Kevin Pearson</h4>
                    <p class="staff-member__title">
                        Assistant Director, Aquatics
                    </p>
                    <p class="staff-member__details">
                        B.S., Biology &amp; Psychology, Trinity University<br />
                        M.E.S.S., Adaptive Physical Education, University of Florida
                    </p>
                    <p class="staff-member__contact">
                        Aquatic Center/Leisure Pool<br />
                        <a href="mailto:kevin.pearson@ttu.edu">kevin.pearson@ttu.edu</a><br />
                        <a href="tel:8067421339">806.742.1339</a> (April - September)<br />
                        <a href="tel:8067423896">806.742.3896</a> (October - March)
                    </p>
                </div>
            </div>
        </section>

        <section class="staff">
          <h2>Coordinators</h2>
          <div class="staff-member">
            <div class="staff-member__image">
              <img src="../images/staff/johanna.gif" alt="Johanna Valencia">
            </div>
            <div class="staff-member__info">
              <h4>Johanna Valencia</h4>
              <p class="staff-member__title">
                Coordinator, Fitness and Wellness
              </p>
              <p class="staff-member__details">
                B.S., Hospitality Administration, Stephen F. Austin University<br />
                M.S., Recreation and Leisure Services, Texas State University, San Marcos
              </p>
              <p class="staff-member__contact">
                Fitness/Wellness Center<br />
                <a href="mailto:Johanna.Valencia@ttu.edu">Johanna.Valencia@ttu.edu</a><br />
                <a href="tel:8067423828">806.742.3828</a>
              </p>
            </div>
          </div>
          <div class="staff-member">
            <div class="staff-member__image">
              <img src="../images/staff/Kevin.gif" alt="Kevin Hoffman">
            </div>
            <div class="staff-member__info">
              <h4>Kevin Hoffman</h4>
              <p class="staff-member__title">coordinator, Outdoor Pursuits</p>
              <p class="staff-member__details">
                B.S., Journalism, University of Montana<br />
                M.S., Sports Health/Sports Administration, Texas Tech University
              </p>
              <p class="staff-member__contact">
                Outdoor Pursuits Center<br />
                <a href="mailto:kevin.h.hoffman@ttu.edu">kevin.h.hoffman@ttu.edu</a><br />
                <a href="tel:8067422949">806.742.2949</a>
              </p>
            </div>
          </div>
        </section>

        <section class="staff">
          <h2>Technology Support</h2>
          <div class="staff-member">
            <div class="staff-member__image">
              <img src="../images/staff/shane.jpg" alt="Shane Sanders">
            </div>
            <div class="staff-member__info">
              <h4>Shane Sanders</h4>
              <p class="staff-member__title">
                it support
              </p>
              <p class="staff-member__details">
                A.A.S., Computer Information Systems, South Plains College
              </p>
              <p class="staff-member__contact">
                Student Rec Center, Rm. 202<br />
                <a href="mailto:shane.sanders@ttu.edu">shane.sanders@ttu.edu</a><br />
                <a href="tel:8067423351">806.742.3351</a>
              </p>
            </div>
          </div>
        </section>

        <section class="staff">
            <h2>Office Assistants</h2>
            <div class="staff-member">
                <div class="staff-member__image">
                    <img src="../images/staff/deborah.gif" alt="Deborah Bowen">
                </div>
                <div class="staff-member__info">
                    <h4>Deborah Bowen</h4>
                    <p class="staff-member__title">
                        Administrative Business Assistant
                    </p>
                    <p class="staff-member__details">
                        Some College AIB courses in banking
                    </p>
                    <p class="staff-member__contact">
                        Student Rec Center, Rm. 202<br />
                        <a href="mailto:Deborah.Bowen@ttu.edu">Deborah.Bowen@ttu.edu</a><br />
                        <a href="tel:8067423351">806.742.3351</a>
                    </p>
                </div>
            </div>
            <div class="staff-member">
                <div class="staff-member__image">
                    <img src="../images/staff/Rita.gif" alt="Rita Brown">
                </div>
                <div class="staff-member__info">
                    <h4>Rita Brown</h4>
                    <p class="staff-member__title">
                        Senior Accounting Processor
                    </p>
                    <p class="staff-member__details">
                        Graduate of Lorenzo High School<br />
                        Took banking courses
                    </p>
                    <p class="staff-member__contact">
                        Student Rec Center, Rm. 202<br />
                        <a href="mailto:Rita.Brown@ttu.edu">Rita.Brown@ttu.edu</a><br />
                        <a href="tel:8067423351">806.742.3351</a>
                    </p>
                </div>
            </div>
            <div class="staff-member">
                <div class="staff-member__image">
                    <img src="../images/staff/elouise.gif" alt="Elouise Medina">
                </div>
                <div class="staff-member__info">
                    <h4>Elouise Medina</h4>
                    <p class="staff-member__title">
                        Senior Business Assistant
                    </p>
                    <p class="staff-member__details">
                        Graduate of Lorenzo High School<br />
                        Took banking courses
                    </p>
                    <p class="staff-member__contact">
                        Student Rec Center, Rm. 202<br />
                        <a href="mailto:Elouise.Medina@ttu.edu">Elouise.Medina@ttu.edu</a><br />
                        <a href="tel:8067423351">806.742.3351</a>
                    </p>
                </div>
            </div>
        </section>
        <!-- CONTENT END -->




        <?php include '../includes/ttu-body-bottom.php'; ?>
    </body>
</html>
