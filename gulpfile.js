"use strict";

// ======================== Load plugins ============================
const autoprefixer  = require("autoprefixer");
const browsersync   = require("browser-sync").create();
const cssnano       = require("cssnano");
const del           = require("del");
const lost          = require("lost");
const gulp          = require("gulp");
const postcss       = require("gulp-postcss");
const rename        = require("gulp-rename");
const sass          = require("gulp-sass");
const plumber       = require("gulp-plumber");
const babel         = require("gulp-babel");
const uglify        = require("gulp-uglify");
const concat        = require("gulp-concat");
const sourcemaps    = require("gulp-sourcemaps");

// ============================ File Paths ==========================
const files =
{
    scssPath:           'stylesheets/scss/**/*.scss',
    jsBabelPath:        ['js/rec.js'],
    phpPath:            '**/*.php',
    concatJSPaths:      [
                            'js/jquery-2.1.1.js',
                            'js/jquery.mobile.custom.js',
                            'js/modernizr.js',
                            'js/scroll.js',
                            'js/classie.js',
                            'js/stickykit.js',
                            'js/wow.js',
                            'js/main.js',
                            'js/slick.min.js',
                            'js/jquery.waitforimages.min.js',
                            'js/jquery.prettyembed.min.js',
                            'js/rec-babel.js'
                        ],
    ouConcatJSPaths:    [
                            'js/slick.min.js',
                            'js/jquery.waitforimages.min.js',
                            'js/jquery.prettyembed.min.js',
                            'js/rec-babel.js'
                        ],
    filesPaths:         [
                            "index.php",
                            "images/**",
                            "_ttu-template/**/*",
                            "includes/**",
                            "stylesheets/fonts/**",
                            "stylesheets/ttu.min.css"
                        ],
    ouFilesPaths:       [
                            "index.php",
                            "images/**",
                            "stylesheets/fonts/**"
                        ]
}

// ==================== Browser Sync functions ======================

/**
* BrowserSync: Initializes the brwoser sync
*/
function browserSync(done)
{
    browsersync.init
    (
        ["stylesheets/**/*.css", "js/**/*.js", "**/*.php"],
        {
            injectChanges: true,
            proxy: "5.5.5.5" // CHANGE THIS URL TO YOUR LOCAL SERVER'S URL
        }
    );
    done();
}

/**
* browserSyncReload: reloads browser
*/
function browserSyncReload(done)
{
    browsersync.reload();
    done();
}

// ====================== Cleaner functions =========================

/**
* clean: Delete dist folder and all compiled outputs css, app.js.
*/
function clean()
{
    return del(['dist', 'stylesheets/styles.css*', 'js/app*.js*']);
}

/**
* ouClean: Delete the ou folder.
* Might give error if the ou folder is in use
*/
function ouClean()
{
    return del(['ou']);
}


// ===================== SCSS related Functions =====================

/**
* scssTask: compiles the style.scss file into style.css
*/
function scssTask()
{
    return gulp
        .src(files.scssPath)
        .pipe(sourcemaps.init())
        .pipe(sass({ outputStyle: "expanded" })) // nested | expanded | compact | compressed
        .pipe(gulp.dest("stylesheets"))
        .pipe(postcss([lost(), autoprefixer()]))
        .pipe(sourcemaps.write("./"))
        .pipe(gulp.dest("stylesheets"))
}

/**
* ouScssTask: compiles the style.scss file into style.css
* destination is for ou deployment
*/
function ouScssTask()
{
    return gulp
        .src(files.scssPath)
        .pipe(sourcemaps.init())
        .pipe(sass({ outputStyle: "expanded" })) // nested | expanded | compact | compressed
        .pipe(gulp.dest("ou/stylesheets"))
        .pipe(postcss([lost(), autoprefixer()]))
        .pipe(sourcemaps.write("./"))
        .pipe(gulp.dest("ou/stylesheets"))
}

/**
* minifyCSS: creates a minified version of the css.
* The output is for dist deployment only
*/
function minifyCSS()
{
    return gulp
        .src('stylesheets/styles.css')
        .pipe(rename({ suffix: ".min" }))
        .pipe(postcss([cssnano()]))
        .pipe(gulp.dest("dist/stylesheets"))
}

/**
* ouMinifyCSS: creates a minified version of the css.
* The output is for ou deployment only
*/
function ouMinifyCSS()
{
    return gulp
        .src('ou/stylesheets/styles.css')
        .pipe(rename({ suffix: ".min" }))
        .pipe(postcss([cssnano()]))
        .pipe(gulp.dest("ou/stylesheets"))
}


//==================== JS Related functions ====================

/**
* babelScripts: creates site-babel.js which can be comptatibile
* with older browsers while writing even if js is written in
* in new format ES6.
*/
function babelScripts()
{
    return gulp
            .src(files.jsBabelPath)
            .pipe(plumber())
            .pipe
            (
                babel
                (
                    {
                        presets: ['@babel/preset-env']
                    }
                )
            )
            // folder only, filename is specified in webpack config
            .pipe(rename('site-babel.js'))
            .pipe(gulp.dest("js"))
            .pipe(browsersync.stream());
}

/**
* concatScripts: Concatenate all the js scripts together
* into one js file. This will include OU template js scripts
* intended for dist deployment. not OU deployment.
*/
function concatScripts()
{
    return gulp
            .src(files.concatJSPaths)
            .pipe(sourcemaps.init())
            .pipe(concat("app.js"))
            .pipe(sourcemaps.write('./'))
            .pipe(gulp.dest('js'))
            .pipe(browsersync.stream());
}

/**
* ouConcatScripts: Concatenate all the js scripts together
* into one js file. This will NOT include OU template js scripts
* intended for OU deployment. not dist deployment.
*/
function ouConcatScripts()
{
    return gulp
            .src(files.ouConcatJSPaths) //different set of js files than dist
            .pipe(sourcemaps.init())
            .pipe(concat("app.js"))
            .pipe(sourcemaps.write('./'))
            .pipe(gulp.dest('ou/js')) //different destination
            .pipe(browsersync.stream());
}

/**
* minifyScripts: Optimizes the combined js script. removes all spaces
* and comments to load faster when viewed in browser. For dist deployment
*/
function minifyScripts()
{
    return gulp
            .src("js/app.js")
            .pipe(uglify({compress: { drop_console: true, hoist_funs: false }}).on('error', function(err) {
               $.util.log($.util.colors.red('[Error]'), err.toString());
               this.emit('end');
               }))
            .pipe(rename({ suffix: ".min" }))
            .pipe(gulp.dest("dist/js"))
}

/**
* ouMinifyScripts: Optimizes the combined js script. removes all spaces
* and comments to load faster when viewed in browser. For dist deployment
*/
function ouMinifyScripts()
{
    return gulp
            .src("ou/js/app.js")
            .pipe(uglify({compress: { drop_console: true, hoist_funs: false }}).on('error', function(err) {
               $.util.log($.util.colors.red('[Error]'), err.toString());
               this.emit('end');
               }))
            .pipe(rename({ suffix: ".min" }))
            .pipe(gulp.dest("ou/js"))
}

// ============================ Copy files ==========================

/**
* copyFiles: copies the index and asset files to dist folder
*/
function copyFiles()
{
    return gulp
        .src(files.filesPaths, { base: './'})
        .pipe(gulp.dest('dist'));
}

/**
* copyFiles: copies the index and asset files to ou folder
*/
function ouCopyFiles()
{
    return gulp
        .src(files.ouFilesPaths, { base: './'})
        .pipe(gulp.dest('ou'));
}

// ==================== watch related functions ====================

function watchFiles()
{
    // Removing the default apache index.html file
    del(['index.html']);

    gulp.watch(files.scssPath, scssTask);
    gulp.watch(files.jsBabelPath, js);
    gulp.watch(files.phpPath, browserSyncReload);
}

//define complex tasks
const js        = gulp.series(babelScripts, concatScripts, browserSyncReload);
const distSass  = gulp.series(scssTask, minifyCSS);
const ouSass    = gulp.series(ouScssTask, ouMinifyCSS);
const distJS    = gulp.series(babelScripts, concatScripts, minifyScripts);
const ouJS      = gulp.series(babelScripts, ouConcatScripts, ouMinifyScripts);
const distBuild = gulp.series(clean, gulp.parallel(distJS, distSass), copyFiles);
const ouBuild   = gulp.series (ouClean, gulp.parallel(ouJS, ouSass) , ouCopyFiles);
const watch     = gulp.series (clean, gulp.parallel(scssTask, js), gulp.parallel(watchFiles, browserSync));

//export tasks
exports.watch       = watch;

exports.distSass    = distSass;
exports.ouSass      = ouSass;

exports.distJS      = distJS;
exports.ouJS        = ouJS;

exports.ou          = ouBuild;
exports.default     = distBuild;
