<!DOCTYPE html>
<html>
    <head>
        <?php include 'includes/ttu-head.html'; ?>
        <title></title>
        <meta name="Description" content="">
    </head>
    <body>
        <?php include 'includes/ttu-body-top.php'; ?>
        <!-- Main content = = = = = = = = = = = = = = = = = = = = = = = -->
        <!--
            If the sidebar is not turned on the following structure is created
            If the sidebar is turned on via properties in OU please refer to file:
            index-with-sidebar.php
        -->
        <section class="l-main-pagecontent">
            <section class="standard-content">
                <div class="row">
                    <div class="large-9 columns large-centered">
                        <h1>Default heading in parameters</h1>
                        <!-- ==================== ADD MAIN CONTENT BELOW THIS LINE ==================== -->
                        <p>Main content goes here</p>
                        <!-- ==================== ADD MAIN CONTENT ABOVE THIS LINE ==================== -->
                    </div>
                </div>
            </section>
        </section>
        <!-- End Main Content = = = = = = = = = = = = = = = = = = = = = -->
        <?php include 'includes/ttu-body-bottom.php'; ?>
    </body>
</html>
